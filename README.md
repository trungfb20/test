Lớp MyBigNumber có một method sum để cộng hai số lớn dưới dạng chuỗi.
Biến result được sử dụng để lưu kết quả của phép cộng.
Biến len1 và len2 lưu độ dài của chuỗi stn1 và stn2.
Biến maxLen lưu độ dài lớn nhất giữa stn1 và stn2.
Biến carry lưu giá trị nhớ của phép cộng.
Trong vòng lặp for, ta lấy từng chữ số của stn1 và stn2 từ phải sang trái, chuyển đổi chúng thành số và cộng lại với giá trị nhớ (nếu có) để tính tổng.
Nếu tổng lớn hơn hoặc bằng 10, ta lưu giá trị 1 vào biến carry để sử dụng cho lần cộng tiếp theo.
Số đơn vị của tổng được thêm vào đầu chuỗi result.
Nếu còn giá trị nhớ ở cuối cùng của phép cộng, ta thêm nó vào đầu chuỗi result.
Cuối cùng, ta trả về kết quả của phép cộng dưới dạng chuỗi.
