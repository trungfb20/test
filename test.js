class MyBigNumber {
  sum(stn1, stn2) {
    let result = "";
    let len1 = stn1.length;
    let len2 = stn2.length;
    let maxLen = Math.max(len1, len2);
    let carry = 0;
    
    for (let i = 0; i < maxLen; i++) {
      let digit1 = i < len1 ? parseInt(stn1.charAt(len1 - 1 - i)) : 0;
      let digit2 = i < len2 ? parseInt(stn2.charAt(len2 - 1 - i)) : 0;
      let sum = digit1 + digit2 + carry;
      
      carry = sum >= 10 ? 1 : 0;
      result = (sum % 10) + result;
    }
    
    if (carry > 0) {
      result = carry + result;
    }
    
    return result;
  }
}
